<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Filter extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		check_admin();
		$this->load->model(['tranlayanan_m','trankamar_m']);
	}

	public function laporan_trankamar()
	{
		$data['row'] = $this->trankamar_m->get();
		$this->template->load('template', 'laporan/laporan_trankamar', $data);
	}
	public function filter_trankamar(){
		$dari = $this->input->post('dari');
		$sampai= $this->input->post('sampai');
		$data['row']=$this->db->query("SELECT * FROM transaksi_kamar tr,tamu tm,kamar km,user us 
		WHERE tr.id_kamar=km.id_kamar and tr.id_tamu=tm.id_tamu and tr.id_user=us.id_user 
		and date(tanggal_checkout) >= '$dari' and date(tanggal_checkout) <= '$sampai' ")->result();
		$this->template->load('template', 'laporan/laporan_kamar', $data);
	}
	public function laporan_tranlayanan()
	{
		$data['row'] = $this->tranlayanan_m->get();
		$this->template->load('template', 'laporan/laporan_tranlayanan', $data);
	}
	public function filter_tranlayanan(){
		$dari = $this->input->post('dari');
		$sampai= $this->input->post('sampai');
		$data['row']=$this->db->query("SELECT * FROM transaksi_layanan tr,user us,transaksi_kamar trk,tamu tm,kamar km,layanan ly
		WHERE  tr.id_user=us.id_user and tr.id_transaksi_kamar=trk.id_transaksi_kamar and tr.id_layanan=ly.id_layanan and trk.id_tamu=tm.id_tamu and trk.id_kamar=km.id_kamar and
		 date(tanggal) >= '$dari' and date(tanggal) <= '$sampai' ")->result();
		$this->template->load('template', 'laporan/laporan_layanan', $data);
	}
}
