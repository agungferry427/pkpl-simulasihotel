<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		check_admin();
        $this->load->model(['layanan_m' , 'kategori_m']);
	}

	public function index()
	{
        $data['row'] = $this->layanan_m->get();
		$this->template->load('template', 'layanan/layanan/layanan_data', $data);
	}
	public function add()
	{
		$layanan = new stdClass();
		$layanan->id_layanan = null;
		$layanan->nama_layanan = null;
		$layanan->harga_layanan = null;
        $layanan->satuan = null;
        
		$query_kategori = $this->kategori_m->get();
        $kategori[null] = '- Pilih -';
        foreach($query_kategori->result() as $kate){
            $kategori[$kate->id_kategori] = $kate->nama_kategori;
        }
		$data = array(
			'page' => 'add',
            'row' => $layanan,
            'kategori' => $kategori, 'selectedkategori' =>null,
		);
		
		$this->template->load('template', 'layanan/layanan/layanan_form' , $data);
	}

	public function edit($id){
		$query = $this->layanan_m->get($id);
		if($query->num_rows() > 0){
			$layanan = $query->row();
            $query_kategori = $this->kategori_m->get();
            $kategori[null] = '- Pilih -';
            foreach($query_kategori->result() as $kate){
                $kategori[$kate->id_kategori] = $kate->nama_kategori;
            }
            $data = array(
                'page' => 'edit',
                'row' => $layanan,
                'kategori' => $kategori, 'selectedkategori' => $layanan->id_kategori,
            );
			
			$this->template->load('template', 'layanan/layanan/layanan_form' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('layanan')."'; </script>"; 
			}

	}

	public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['add'])){
			$this->layanan_m->add($post);
		}else if(isset($_POST['edit'])){
			$this->layanan_m->edit($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('layanan')."'; </script>"; 
	}

	public function del($id)
	{
		$this->layanan_m->del($id);
		if($this->db->affected_rows() >0 ){
			echo "<script>alert('Data Berhasil Dihapus');</script>";
		}
		echo "<script>window.location='".site_url('layanan')."';</script>";
	}
}
