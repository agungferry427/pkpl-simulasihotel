<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tamu extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		check_admin_fo();
		$this->load->model('tamu_m');
	}

	public function index()
	{
		$data['row'] = $this->tamu_m->get();
		$this->template->load('template', 'tamu/tamu_data', $data);
	}
	public function add()
	{
		$tamu = new stdClass();
		$tamu->id_tamu = null;
		$tamu->nama_tamu = null;
		$tamu->prefix = null;
		$tamu->tipe_identitas = null;
		$tamu->no_identitas = null;
		$tamu->wn_tamu = null;
		$tamu->alamat = null;
		$tamu->hp_tamu = null;
		$tamu->email_tamu = null;
		$data = array(
			'page' => 'add',
			'row' => $tamu
		);
		
		$this->template->load('template', 'tamu/tamu_form' , $data);
	}

	public function edit($id){
		$query = $this->tamu_m->get($id);
		if($query->num_rows() > 0){
			$tamu = $query->row();
			$data = array(
				'page' => 'edit',
				'row' => $tamu

			);
			$this->template->load('template', 'tamu/tamu_form' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('tamu')."'; </script>"; 
			}

	}

	public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['add'])){
			$this->tamu_m->add($post);
		}else if(isset($_POST['edit'])){
			$this->tamu_m->edit($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('tamu')."'; </script>"; 
	}

	public function del($id)
	{
		$this->tamu_m->del($id);
		if($this->db->affected_rows() >0 ){
			echo "<script>alert('Data Berhasil Dihapus');</script>";
		}
		echo "<script>window.location='".site_url('tamu')."';</script>";
	}
}
