<?php defined('BASEPATH') or exit('No direct script access allowed');

class Checkout extends CI_Controller
{
    function __construct()
    {	
        parent::__construct();
		check_not_login();
		check_admin_fo();
        $this->load->model(['kamar_m', 'tipe_m','tranlayanan_m', 'trankamar_m', 'user_m', 'tamu_m']);
    }

    public function index()
    {
       
        $data['row'] = $this->trankamar_m->get();
        $this->template->load('template', 'checkout/checkout', $data);
    }

   
    public function edit($id){
        $query= $this->trankamar_m->get($id);
      
		if($query->num_rows() > 0){
			$trankamar = $query->row();
			$tipe = $this->tipe_m->get();
			$tamu = $this->tamu_m->get();
            $kamar =$this->kamar_m->get();
        
            $layanan = $this->trankamar_m->nilaijoin($id);
			$data = array(
				'page' => 'edit',
				'row' => $trankamar,
				'tipe' => $tipe,
				'tamu' => $tamu,
				'kamar' => $kamar,
                'layanan' =>$layanan,
          
            );
			
			$this->template->load('template', 'checkout/proses_checkout' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('checkin/tampil_data')."'; </script>"; 
			}
	}
	public function print($id){
		$query= $this->trankamar_m->get($id);
      
		if($query->num_rows() > 0){
			$trankamar = $query->row();
			$tipe = $this->tipe_m->get();
			$tamu = $this->tamu_m->get();
            $kamar =$this->kamar_m->get();
        
            $layanan = $this->trankamar_m->nilaijoin($id);
			$data = array(
				'page' => 'edit',
				'row' => $trankamar,
				'tipe' => $tipe,
				'tamu' => $tamu,
				'kamar' => $kamar,
                'layanan' =>$layanan,
          
            );
			
			$this->template->load('template','checkout/print' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('checkin/tampil_data')."'; </script>"; 
			}
	}
  
	public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['add'])){
			$this->trankamar_m->add($post);
		}else if(isset($_POST['edit'])){
			$this->trankamar_m->edit($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('checkin/tampil_data')."'; </script>"; 
	}
}
