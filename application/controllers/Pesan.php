<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pesan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        check_not_login();
        check_admin_fo();
        $this->load->model(['kamar_m', 'tipe_m', 'trankamar_m', 'user_m', 'tamu_m','kategori_m','layanan_m','tranlayanan_m']);
    }

    public function index()
    {
        $data['row'] = $this->trankamar_m->get();
        $this->template->load('template', 'pesan/pesan', $data);
    }

    public function pesan_layanan($id)
	{
        $query= $this->trankamar_m->get($id);
        if($query->num_rows() > 0){
            $tranlayanan = new StdClass();
            $tranlayanan->id_transaksi_layanan = null;
            $tranlayanan->total = null;
            $tranlayanan->waktu = null;
            $tranlayanan->tanggal = null;
            $tranlayanan->jumlah = null;


            $transaksi= $query->row();
            $kategori = $this->kategori_m->get();
            $layanan = $this->layanan_m->get();
			$data = array(
                'page' => 'pesan_layanan',
                'penting'=>$tranlayanan,
                'row' => $transaksi,
                'kategori' => $kategori,
                'layanan' => $layanan,
			);
			
            $this->template->load('template', 'pesan/transaksi_layanan', $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('pesan')."'; </script>"; 
			}
    }
    
    public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['pesan_layanan'])){
			$this->tranlayanan_m->add($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('pesan')."'; </script>"; 
    }
    

// Back-Up 

    
}


