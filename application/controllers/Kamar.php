<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
        $this->load->model(['kamar_m' , 'tipe_m']);
	}

	public function index()
	{
        $data['row'] = $this->kamar_m->get();
		$this->template->load('template', 'Kamar/Kamar/kamar_data', $data);
	}
	public function info()
	{
		$data['dataif'] = $this->kamar_m->data_if();
        $data['row'] = $this->kamar_m->get();
		$this->template->load('template', 'Kamar/Kamar/kamar_info', $data);
	}
	public function add()
	{	
		check_admin_hk();
		$kamar = new stdClass();
		$kamar->id_kamar = null;
		$kamar->nama_kamar = null;
		$kamar->harga_malam = null;
        $kamar->keterangan = null;
        
        $tipe = $this->tipe_m->get();
		$data = array(
			'page' => 'add',
            'row' => $kamar,
            'tipe' => $tipe,
		);
		
		$this->template->load('template', 'Kamar/Kamar/kamar_form' , $data);
	}

	public function edit($id){
		check_admin_hk();
		$query = $this->kamar_m->get($id);
		if($query->num_rows() > 0){
			$kamar = $query->row();
			$tipe = $this->tipe_m->get();
			$data = array(
				'page' => 'edit',
				'row' => $kamar,
				'tipe' => $tipe,
			);
			
			$this->template->load('template', 'Kamar/Kamar/kamar_edit' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('kamar')."'; </script>"; 
			}

	}

	public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['add'])){
			$this->kamar_m->add($post);
		}else if(isset($_POST['edit'])){
			$this->kamar_m->edit($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('kamar')."'; </script>"; 
	}

	public function del($id)
	{
		$this->kamar_m->del($id);
		if($this->db->affected_rows() >0 ){
			echo "<script>alert('Data Berhasil Dihapus');</script>";
		}
		echo "<script>window.location='".site_url('kamar')."';</script>";
	}
}
