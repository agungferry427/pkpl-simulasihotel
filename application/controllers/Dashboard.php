<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		check_not_login();
		$this->load->model(['kamar_m' , 'tipe_m','user_m','tamu_m','trankamar_m']);
		$data['row'] = $this->kamar_m->get();
		$data['dataif'] = $this->kamar_m->data_if();
		$data['user'] = $this->user_m->hitunguser();
		$data['tamu'] = $this->tamu_m->hitungtamu();
		$data['trankamar'] = $this->trankamar_m->get();
		$this->template->load('template', 'dashboard', $data);
	}
}
