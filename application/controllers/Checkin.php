<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		
        $this->load->model(['kamar_m' , 'tipe_m' , 'trankamar_m' , 'user_m' , 'tamu_m']);
	}

	public function index()
	{	check_admin_fo();
		$data['row'] = $this->kamar_m->get();
		$this->template->load('template', 'checkin/checkin', $data);
	}
	public function tampil_data()
	{	

		$data['row'] = $this->trankamar_m->get();
		$this->template->load('template', 'checkin/checkin_data', $data);
	}

	public function add($id){
		check_admin_fo();
		$query = $this->kamar_m->get($id);
		if($query->num_rows() > 0){
			$transaksi= new stdClass();
			$transaksi->id_transaksi_kamar = null;
			$transaksi->nomor_invoice = null;
			$transaksi->jumlah_dewasa = null;
			$transaksi->jumlah_anak = null;
			$transaksi->tanggal_checkin = null;
			$transaksi->tanggal_checkout = null;
			$transaksi->waktu_checkin = null;
			$transaksi->waktu_checkout = null;
			$transaksi->total_biaya_kamar = null;
			$transaksi->deposit = null;
			$transaksi->status = null;
			
			$kamar = $query->row();
			$tipe = $this->tipe_m->get();
			$tamu = $this->tamu_m->get();
			
			$data = array(
				'page' => 'add',
				'row' => $kamar,
				'penting' => $transaksi,
				'tipe' => $tipe,
				'tamu' => $tamu,
			);
			
			$this->template->load('template', 'checkin/proses_checkin' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('checkin')."'; </script>"; 
			}
	}
	public function edit($id){
		check_admin_fo();
		$query = $this->trankamar_m->get($id);
		if($query->num_rows() > 0){
			$trankamar = $query->row();
			$tipe = $this->tipe_m->get();
			$tamu = $this->tamu_m->get();
			$kamar =$this->kamar_m->get();
			$data = array(
				'page' => 'edit',
				'row' => $trankamar,
				'tipe' => $tipe,
				'tamu' => $tamu,
				'kamar' => $kamar,
			);
			
			$this->template->load('template', 'checkin/proses_checkin_update' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('checkin/tampil_data')."'; </script>"; 
			}
	}
	public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['add'])){
			$this->trankamar_m->add($post);
		}else if(isset($_POST['edit'])){
			$this->trankamar_m->edit($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('checkin/tampil_data')."'; </script>"; 
	}
}
