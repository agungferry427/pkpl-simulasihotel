<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login()
	{
		check_already_login();
		$this->load->helper('captcha');
		$vals = array(
			'word'          => substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6),
			'img_path'      => './assets/images/',
			'img_url'       => base_url('assets/images/'),
			'img_width'     => '150',
			'img_height'    => 30,
			'expiration'    => 7200,
			'word_length'   => 5,
			'font_size'     => 25,
			'img_id'        => 'Imageid',
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
	
			// White background and border, black text and red grid
			'colors'        => array(
					'background' => array(255, 255, 255),
					'border' => array(255, 255, 255),
					'text' => array(0, 0, 0),
					'grid' => array(255, 200, 200)
			)
		);
		
		$cap = create_captcha($vals);
		$image = $cap['image'];
		$captchaword = $cap['word'];
		$this->session->set_userdata('captchaword',$captchaword);
		$this->load->view('login',['captcha_image'=>$image]);
	}

	public function process()
	{	
		$captcha = $this->input->post('captcha');

		$captcha_answer = $this->session->userdata('captchaword');

		if($captcha==$captcha_answer)
		{
			$post = $this->input->post(null, TRUE);
			if(isset($post['login'])){
			$this->load->model('user_m');
			$query = $this->user_m->login($post);
				if($query->num_rows() > 0 ){
					$row = $query->row();
					$params = array(
						'id_user' => $row->id_user,
						'level' => $row->level
					);
					$this->session->set_userdata($params);
					echo "<script>
						alert('Selamat , Anda berhasil login');
						window.location='".site_url('dashboard')."';
					</script>";
				}else{
					echo "<script>
						alert('Login Gagal , Silahkan ulang');
						window.location='".site_url('auth/login')."';
					</script>";
				}
			}
		}
		else{
			$this->session->set_flashdata('error','<div class="alert alert-danger">Captcha Salah</div>');
			redirect('auth/login');
		}

		// $post = $this->input->post(null, TRUE);
		// if(isset($post['login'])){
		// 	$this->load->model('user_m');
		// 	$query = $this->user_m->login($post);
		// 	if($query->num_rows() > 0 ){
		// 		$row = $query->row();
		// 		$params = array(
		// 			'id_user' => $row->id_user,
		// 			'level' => $row->level
		// 		);
		// 		$this->session->set_userdata($params);
		// 		echo "<script>
		// 			alert('Selamat , Anda berhasil login');
		// 			window.location='".site_url('dashboard')."';
		// 		</script>";
		// 	}else{
		// 		echo "<script>
		// 			alert('Login Gagal , Silahkan ulang');
		// 			window.location='".site_url('auth/login')."';
		// 		</script>";
		// 	}
		// }

	}

	public function logout(){
		$params = array('id_user', 'level');
		$this->session->unset_userdata($params);
		redirect('auth/login');
	}
}
