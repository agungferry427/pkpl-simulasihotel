<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		check_admin_hk();
		$this->load->model('tipe_m');
	}

	public function index()
	{
		$data['row'] = $this->tipe_m->get();
		$this->template->load('template', 'Kamar/Tipe/tipe_data', $data);
	}
	public function add()
	{
		$tipe = new stdClass();
		$tipe->id_tipe = null;
		$tipe->nama_tipe = null;
		$tipe->harga_malam = null;
		$tipe->keterangan = null;
		$data = array(
			'page' => 'add',
			'row' => $tipe
		);
		
		$this->template->load('template', 'Kamar/Tipe/tipe_form' , $data);
	}

	public function edit($id){
		$query = $this->tipe_m->get($id);
		if($query->num_rows() > 0){
			$tipe = $query->row();
			$data = array(
				'page' => 'edit',
				'row' => $tipe

			);
			$this->template->load('template', 'Kamar/Tipe/tipe_form' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('tipe')."'; </script>"; 
			}

	}

	public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['add'])){
			$this->tipe_m->add($post);
		}else if(isset($_POST['edit'])){
			$this->tipe_m->edit($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('tipe')."'; </script>"; 
	}

	public function del($id)
	{
		$this->tipe_m->del($id);
		if($this->db->affected_rows() >0 ){
			echo "<script>alert('Data Berhasil Dihapus');</script>";
		}
		echo "<script>window.location='".site_url('tipe')."';</script>";
	}
}
