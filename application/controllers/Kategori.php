<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		check_admin();
        $this->load->model('kategori_m');
	}

	public function index()
	{
        $data['row'] = $this->kategori_m->get();
		$this->template->load('template', 'layanan/kategori/kategori_data', $data);
	}
	public function add()
	{
		$kategori = new stdClass();
		$kategori->id_kategori = null;
		$kategori->nama_kategori = null;
        $kategori->keterangan = null;
        
   
		$data = array(
			'page' => 'add',
            'row' => $kategori,
		);
		
		$this->template->load('template', 'layanan/kategori/kategori_form' , $data);
	}

	public function edit($id){
		$query = $this->kategori_m->get($id);
		if($query->num_rows() > 0){
			$kategori = $query->row();
			$tipe = $this->kategori_m->get();
			$data = array(
				'page' => 'edit',
				'row' => $kategori,
				'tipe' => $tipe,
			);
			
			$this->template->load('template', 'layanan/kategori/kategori_form' , $data);
			}else{
				echo "<script> alert('Data Tidak ditemukan');";
				echo "window.location='".site_url('kategori')."'; </script>"; 
			}

	}

	public function procces(){
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['add'])){
			$this->kategori_m->add($post);
		}else if(isset($_POST['edit'])){
			$this->kategori_m->edit($post);
		}
		if($this->db->affected_rows() > 0){
			echo "<script> alert('Data Berhasil Disimpan'); </script>";
		}
		echo "<script>window.location='".site_url('kategori')."'; </script>"; 
	}

	public function del($id)
	{
		$this->kategori_m->del($id);
		if($this->db->affected_rows() >0 ){
			echo "<script>alert('Data Berhasil Dihapus');</script>";
		}
		echo "<script>window.location='".site_url('kategori')."';</script>";
	}
}
