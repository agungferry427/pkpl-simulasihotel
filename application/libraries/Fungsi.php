<?php

Class Fungsi {

    protected $ci;

    function __construct(){
        $this->ci =& get_instance();
    }

    function user_login() {
        $this->ci->load->model('user_m');
        $id_user = $this->ci->session->userdata('id_user');
        $user_data = $this->ci->user_m->get($id_user)->row();
        return $user_data;
    }
    public function count_item(){
        $this->ci->load->model('kamar_m');
        return $this->ci->kamar_m->get()->num_rows();
    }
  
}