<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('kategori_layanan');
        if($id != null){
            $this->db->where('id_kategori', $id);
        }
        $query = $this->db->get();
        return $query;
    }
    public function add($post){
        $parrams = [
            'nama_kategori'  => $post['namakategori'],
            'keterangan' => $post['keterangan'],
        ];
        $this->db->insert('kategori_layanan', $parrams);
    }
    public function edit($post){
        $parrams = [
            'nama_kategori'  => $post['namakategori'],
            'keterangan' => $post['keterangan'],
        ];
        $this->db->where('id_kategori', $post['id']);
        $this->db->update('kategori_layanan', $parrams);
    }
    public function del($id){
        $this->db->where('id_kategori',$id);
        $this->db->delete('kategori_layanan');
    }
}