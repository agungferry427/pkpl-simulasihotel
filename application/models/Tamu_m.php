<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tamu_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('tamu');
        if($id != null){
            $this->db->where('id_tamu', $id);
        }
        $query = $this->db->get();
        return $query;
    }
    public function hitungtamu()
	{   
		$query = $this->db->get('tamu');
			if($query->num_rows()>0)
			{
				return $query->num_rows();
			}
			else
			{
			return 0;
			}
	}
    public function add($post){
        $parrams = [
            'prefix' => $post['panggilan'],
            'nama_tamu'  => $post['namatamu'],
            'tipe_identitas' => $post['identitas'],
            'no_identitas'  => $post['noi'],
           'wn_tamu' => $post['wn'],
           'alamat'  => $post['alamat'],
           'hp_tamu'   => $post['nohp'],
            'email_tamu' => $post['email'],
        ];
        $this->db->insert('tamu', $parrams);
    }
    public function edit($post){
        $parrams = [
            'prefix' => $post['panggilan'],
            'nama_tamu'  => $post['namatamu'],
            'tipe_identitas' => $post['identitas'],
            'no_identitas'  => $post['noi'],
           'wn_tamu' => $post['wn'],
           'alamat'  => $post['alamat'],
           'hp_tamu'   => $post['nohp'],
            'email_tamu' => $post['email'],
        ];
        $this->db->where('id_tamu', $post['id']);
        $this->db->update('tamu', $parrams);
    }
    public function del($id){
        $this->db->where('id_tamu', $id);
        $this->db->delete('tamu');
    }
}