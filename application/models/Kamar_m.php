<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('kamar');
        $this->db->join('tipe_kamar' , 'tipe_kamar.id_tipe = kamar.id_tipe');
        if($id != null){
            $this->db->where('id_kamar', $id);
        }
        $query = $this->db->get();
        return $query;
    }
    
	public function data_if(){
        $sql = "SELECT  count(if( status_kamar = 'Vacant Clean Inspected', status_kamar, null)) as VCI,
                        count(if( status_kamar = 'Vacant Clean', status_kamar, null))as VC,
                        count(if( status_kamar = 'Vacant Dirty', status_kamar, null)) as VD,
                        count(if( status_kamar = 'Occupied', status_kamar, null))as OCC,
                        count(if( status_kamar = 'Occupied Clean', status_kamar, null)) as OC,
                        count(if( status_kamar = 'Occupied Dirty', status_kamar, null))as OD,
                        count(if( status_kamar = 'Out of Order', status_kamar, null))as OOO
                        
         from kamar";
        $result = $this->db->query($sql);
        return $result->row();
    }

    public function add($post){
        $parrams = [
            'no_kamar' => $post['no_kamar'],
            'max_dewasa' => $post['maxdew'],
            'max_anak'  => $post['maxanak'],
            'id_tipe' => $post['tipe'],
            'status_kamar' => $post['status'],
        ];
        $this->db->insert('kamar', $parrams);
    }
    public function edit($post){
        $parrams = [
            'no_kamar' => $post['no_kamar'],
            'max_dewasa' => $post['maxdew'],
            'max_anak'  => $post['maxanak'],
            'id_tipe' => $post['tipe'],
            'status_kamar' => $post['status'],
        ];
        $this->db->where('id_kamar', $post['id']);
        $this->db->update('kamar', $parrams);
    }
    public function del($id){
        $this->db->where('id_kamar', $id);
        $this->db->delete('kamar');
    }
}