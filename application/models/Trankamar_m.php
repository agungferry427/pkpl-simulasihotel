<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Trankamar_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('transaksi_kamar');
        $this->db->join('kamar' , 'kamar.id_kamar = transaksi_kamar.id_kamar');
        $this->db->join('tipe_kamar' , 'tipe_kamar.id_tipe = kamar.id_tipe');
        $this->db->join('tamu' , 'tamu.id_tamu = transaksi_kamar.id_tamu');
        $this->db->join('user' , 'user.id_user = transaksi_kamar.id_user');
        
        
        if($id != null){
           
            $this->db->where('id_transaksi_kamar', $id);
            
         
        }
        $query = $this->db->get();
        return $query;
    }
    function nilaijoin($id){
        
        $this->db->from('transaksi_layanan as b'); 
        $this->db->join('transaksi_kamar as a', 'a.id_transaksi_kamar = b.id_transaksi_kamar');
        $this->db->join('layanan' , 'layanan.id_layanan = b.id_layanan');
		$this->db->where('b.id_transaksi_kamar',$id);
		$result= $this->db->get();
		return $result;
	}
    public function add($post){
       
        $parrams = [
            'id_transaksi_kamar' => $post['idt'],
            'id_user' => $post['iduser'],
            'id_kamar' => $post['id'],
            'id_tamu'  => $post['tamuinap'],
            'nomor_invoice' => $post['nomor_invoice'],
            'jumlah_dewasa' => $post['jumlah_dewasa'],
            'jumlah_anak' => $post['jumlah_anak'],
            'tanggal_checkin' => $post['tanggal_checkin'],
            'tanggal_checkout' => $post['tanggal_checkout'],
            'waktu_checkout' => $post['waktu_checkout'],
            'waktu_checkin' => $post['waktu_checkin'],
            'deposit' => $post['deposit'],
            'status' => $post['statustamu'],

        ];
        //  $this->db->join('kamar' , 'kamar.id_kamar = transaksi_kamar.id_kamar');
         $kamar = ['status_kamar' => $post['statuskamar']];
         $this->db->where('id_kamar', $post['id']);
         $this->db->update('kamar', $kamar);  
        //  $kamarcoba = ['status_kamar' => $post['statuskamarcoba']];
        //  $this->db->where('id_kamar',$post['id']);
        //  $this->db->where(date('y-m-d'),$post['tanggal_besok']);
        //  $this->db->update('kamar', $kamarcoba);  
        $this->db->insert('transaksi_kamar', $parrams);
    }
    public function edit($post){
        $parrams = [
            'id_user' => $post['iduser'],
            'id_kamar' => $post['id'],
            'id_tamu'  => $post['tamuinap'],
            'nomor_invoice' => $post['nomor_invoice'],
            'jumlah_dewasa' => $post['jumlah_dewasa'],
            'jumlah_anak' => $post['jumlah_anak'],
            'tanggal_checkin' => $post['tanggal_checkin'],
            'tanggal_checkout' => $post['tanggal_checkout'],
            'waktu_checkout' => $post['waktu_checkout'],
            'waktu_checkin' => $post['waktu_checkin'],
            'total_biaya_kamar' =>$post['harga_kamar'],
            'deposit' => $post['deposit'],
            'status' => $post['statustamu'],
        ];
        $kamar = ['status_kamar' => $post['statuskamar']];
        $this->db->where('id_kamar', $post['id']);
        $this->db->update('kamar', $kamar);
        $this->db->where('id_transaksi_kamar', $post['idt']);
        $this->db->update('transaksi_kamar', $parrams);
    }
    
    public function del($id){
        $this->db->where('id_kamar', $id);
        $this->db->delete('kamar');
    }
}