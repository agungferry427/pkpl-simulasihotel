<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tranlayanan_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('transaksi_layanan');
        $this->db->join('transaksi_kamar' , 'transaksi_kamar.id_transaksi_kamar = transaksi_layanan.id_transaksi_kamar');
        $this->db->join('layanan' , 'layanan.id_layanan = transaksi_layanan.id_layanan');
        $this->db->join('user' , 'user.id_user = transaksi_layanan.id_user');
        $this->db->join('tamu' , 'tamu.id_tamu = transaksi_kamar.id_tamu');
        $this->db->join('kamar' , 'kamar.id_kamar = transaksi_kamar.id_kamar');
     
        if($id != null){
            $this->db->where('id_transaksi_layanan', $id);
         
        }
        $query = $this->db->get();
        return $query;
    }
  
    function nilaijoin($id){
    
        $this->db->from('layanan as b');  
		$this->db->join('kategori_layanan as a', 'a.id_kategori = b.id_kategori');
		$this->db->where('b.id_kategori',$id);
		$result= $this->db->get();
		return $result;
	}
    public function add($post){
        $this->db->join('layanan' , 'layanan.id_layanan = transaksi_layanan.id_layanan');
        $parrams = [
            'id_transaksi_kamar' => $post['idt'],
            'id_user' => $post['iduser'],
            'id_transaksi_layanan' => $post['idl'],
            'id_layanan' => $post['idlayanan'],
            'tanggal' => $post['tanggal'],
            'waktu' => $post['waktu'],
            'total' => $post['jumlah']*$post['harga_layanan'],
            'jumlah' => $post['jumlah'],

        ];
        $this->db->insert('transaksi_layanan', $parrams);
    }

    public function edit($post){
        $parrams = [
            'id_user' => $post['iduser'],
            'id_kamar' => $post['id'],
            'id_tamu'  => $post['tamuinap'],
            'nomor_invoice' => $post['nomor_invoice'],
            'jumlah_dewasa' => $post['jumlah_dewasa'],
            'jumlah_anak' => $post['jumlah_anak'],
            'tanggal_checkin' => $post['tanggal_checkin'],
            'tanggal_checkout' => $post['tanggal_checkout'],
            'waktu_checkout' => $post['waktu_checkout'],
            'waktu_checkin' => $post['waktu_checkin'],
            'deposit' => $post['deposit'],
            'status' => $post['statustamu'],
        ];
        $this->db->where('id_transaksi_kamar', $post['idt']);
        $this->db->update('transaksi_kamar', $parrams);
    }
    public function del($id){
        $this->db->where('id_kamar', $id);
        $this->db->delete('kamar');
    }
}