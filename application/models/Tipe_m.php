<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('tipe_kamar');
        if($id != null){
            $this->db->where('id_tipe', $id);
        }
        $query = $this->db->get();
        return $query;
    }
    public function add($post){
        $parrams = [
            'nama_tipe' => $post['tipekamar'],
            'harga_malam'  => $post['hargamalam'],
            'keterangan' => $post['keterangan'],
        ];
        $this->db->insert('tipe_kamar', $parrams);
    }
    public function edit($post){
        $parrams = [
            'nama_tipe' => $post['tipekamar'],
            'harga_malam'  => $post['hargamalam'],
            'keterangan' => $post['keterangan'],
        ];
        $this->db->where('id_tipe', $post['id']);
        $this->db->update('tipe_kamar', $parrams);
    }
    public function del($id){
        $this->db->where('id_tipe', $id);
        $this->db->delete('tipe_kamar');
    }
}