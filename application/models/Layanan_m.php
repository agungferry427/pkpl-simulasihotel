<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('layanan');
        $this->db->join('kategori_layanan' , 'kategori_layanan.id_kategori = layanan.id_kategori');
        if($id != null){
            $this->db->where('id_layanan', $id);
        }
        $query = $this->db->get();
        return $query;
    }
  
    public function add($post){
        $parrams = [
            'nama_layanan' => $post['namalayanan'],
            'id_kategori' => $post['kategori'],
            'satuan'  => $post['satuan'],
            'harga_layanan' => $post['hargalayanan'],
        ];
        $this->db->insert('layanan', $parrams);
    }
    public function edit($post){
        $parrams = [
            'nama_layanan' => $post['namalayanan'],
            'id_kategori' => $post['kategori'],
            'satuan'  => $post['satuan'],
            'harga_layanan' => $post['hargalayanan'],
        ];
        $this->db->where('id_layanan', $post['id']);
        $this->db->update('layanan', $parrams);
    }
    public function del($id){
        $this->db->where('id_layanan', $id);
        $this->db->delete('layanan');
    }
}