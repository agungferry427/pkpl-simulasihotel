<?php
    date_default_timezone_set('Asia/Jakarta');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Room Service</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Pilih produk/layanan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">

      <div class="pull-right">
        <a href="<?= site_url('pesan') ?>" class="float-right btn btn-warning">
          Batal
        </a>
      </div>
      <h3 class="card-title">Pesanan Kamar : <b><?=$row->no_kamar?>-<?=$row->nama_tamu?></b></h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <table class="table table-striped">
        <thead>
          <tr>
            
            <th>Kategori Layanan</th>
            <th>Nama Layanan</th>
            <th>Harga Layanan</th>
            <th>Jumlah Pesanan</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
              foreach ($layanan->result() as $key => $data) {
          ?>
          <form action="<?= site_url('pesan/procces') ?>" method="post">
         
              <tr>
                <input type="hidden" name='iduser' value="<?= $this->fungsi->user_login()->id_user ?>">
                <input type="hidden" name='idl' value="<?= $penting->id_transaksi_layanan?>">
                <input type="hidden" name="tanggal" value="<?php echo date('Y-m-d'); ?>">
                <input type="hidden" name="waktu" value="<?php echo date('H:i'); ?>">
                <input type="hidden" name='idt' value="<?=$row->id_transaksi_kamar?>">
                <input type="hidden" name='idlayanan' value="<?=$data->id_layanan?>">
                <input type="hidden" name='total' value="<?=$penting->total?>">
                <input type="hidden" name='harga_layanan' value="<?=$data->harga_layanan?>">
                  <td><?=$data->nama_kategori?></td>
                  <td><?= $data->nama_layanan ?></td>
                  <td><?= $data->harga_layanan ?> / <?= $data->satuan ?></td>
                  <td>
                    <div class="row">
                      <div class="col-sm-4">
                        <input class="form-control" name="jumlah" />
                      </div>
                      <div class="col-sm-6">
                        <?= $data->satuan ?>
                      </div>
                    </div>
                  </td>
                  <td>
                    <button class="btn btn-xs btn-success" name="<?= $page ?>" type="submit">Pesan</button>
                  </td>
              </tr>
          </form>
          <?php
            }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->