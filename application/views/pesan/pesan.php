<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Kamar</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Kamar</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="box">
        <div class="box-body">
       
                <div class="row">
                        <?php
                        foreach ($row->result() as $key => $data) { 
                            if ($data->status == "Check-in") {
                        ?>
                        <div class="col-sm-3">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3><?=$data->no_kamar ?></h3>
                                    <p><?=$data->prefix?>.<?=$data->nama_tamu?></p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-bed"></i>
                                </div>
                                <a class="small-box-footer" href="<?= site_url('pesan/pesan_layanan/').$data->id_transaksi_kamar?>">Pilih Kamar</a>
                            </div>
                        </div>
                    <?php  }}?>
                </div>
      
        </div>
    </div>
</section>