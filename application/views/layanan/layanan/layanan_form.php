<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Layanan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Layanan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <!-- general form elements -->
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title"><?= ucfirst($page) ?> layanan</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="<?= site_url('layanan/procces') ?>" method="post">
      <div class="card-body">
        <div class="form-group">
          <label>Nama Layanan / Produk</label>
          <input type="hidden" name='id' value="<?= $row->id_layanan ?>">
          <input type="text" name="namalayanan" class="form-control" value="<?= $row->nama_layanan ?>" placeholder="Enter ..." required>
        </div>
        <div class="form-group">
          <label>Kategori Layanan</label>
          <?php echo form_dropdown(
            'kategori',
            $kategori,
            $selectedkategori,
            ['class' => 'form-control', 'required' => 'required']
          ) ?>

        </div>
        <div class="form-group">
          <label>Satuan</label>
          <input type="text" name="satuan" value="<?= $row->satuan ?>" class="form-control" placeholder="Enter ...">
        </div>
        <div class="form-group">
          <label>Harga</label>
          <input type="number" name="hargalayanan" value="<?= $row->harga_layanan ?>" class="form-control" placeholder="Enter ...">
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" name="<?= $page ?>" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <!-- /.card -->


</section>
<!-- /.content -->