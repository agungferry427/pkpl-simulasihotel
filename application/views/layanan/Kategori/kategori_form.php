
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tamu</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Tamu</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title"><?=ucfirst($page)?> Tipe Kamar</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="<?=site_url('kategori/procces')?>" method="post">
                <div class="card-body"> 
                    <div class="form-group">
                            <label>Nama Kategori Layanan</label>
                            <input type="hidden" name='id' value="<?=$row->id_kategori?>">
                            <input type="text" name="namakategori" class="form-control" value="<?=$row->nama_kategori?>"  placeholder="Enter ..." required >
                    </div>
                    <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control"><?=$row->keterangan?></textarea>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" name="<?=$page?>"class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card -->


</section>
<!-- /.content -->
 