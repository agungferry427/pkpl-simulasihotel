<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Laporan Transaksi Layanan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item">Laporan</a></li>
          <li class="breadcrumb-item active">Laporan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">           
                <h3 class="card-title">Filter Data </h3>
          </div>
          <div class="card card-primary">
              <form  action="<?=site_url('filter/filter_tranlayanan')?>" method="post">
                  <div class="card-body">
                      <div class="form-group">
                              <label>Dari tanggal</label>
                              <input type="date" name="dari"  class="form-control  " placeholder="Enter ..." required>
                              
                          </div>
                      <div class="form-group">
                              <label>Sampai tanggal</label>
                              <input type="date" name="sampai"  class="form-control  " placeholder="Enter ..." required>
                            
                      </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Tampilkan Data</button>
                  </div>
              </form>
          </div>
        </div>
      </div>
  </div>
 <div class="row">
    <div class="col-12">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">           
                <div class="pull-right">
                    <a class="float-right btn btn-warning" href="<?= site_url('filter/laporan_trankamar') ?>">Kembali</a>
                </div>
                <h3 class="card-title">Transaksi Kamar </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <table id="table1" class="table table-striped">
                <thead>
                <tr>
                    <th>Nama Operator</th>
                    <th>Tanggal/Waktu</th>
                    <th>Nomor Kamar</th>
                    <th>Nama Tamu</th>
                    <th>Produk Layanan</th>
                    <th>Harga Satuan</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                </tr>
                </thead>

                <tbody>
                <?php
                foreach ($row->result() as $key => $data) {
                ?>
                    <tr>
                    
                        <td><?= $data->nama ?></td>
                        <td><?= $data->tanggal?> / <?= $data->waktu?> </td>
                        <td><?= $data->no_kamar ?></td>
                        <td><?= $data->nama_tamu ?></td>
                        <td><?= $data->nama_layanan?></td>
                        <td><?= $data->harga_layanan ?> /<?= $data->satuan ?>  </td>
                        <td><?= $data->jumlah ?></td>
                        <td><?= $data->total ?></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
            </div>
            <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->