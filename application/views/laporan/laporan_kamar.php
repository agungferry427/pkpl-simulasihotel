<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Laporan Transaksi Kamar</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item">Laporan</a></li>
          <li class="breadcrumb-item active">Laporan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<section class="content">
 <div class="row">
    <div class="col-12">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">           
                <div class="pull-right">
                    <a class="float-right btn btn-warning" href="<?= site_url('filter/laporan_trankamar') ?>">Kembali</a>
                </div>
                <h3 class="card-title">Transaksi Kamar </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <table id="table1" class="table table-striped">
                <thead>
                <tr>
                    <th>Nama Operator</th>
                    <th>Nomor Invoice</th>
                    <th>Tanggal</th>
                    <th>Nomor Kamar</th>
                    <th>Nama Tamu</th>
                    <th>Total Harga</th>
                </tr>
                </thead>

                <tbody>
                <?php
                foreach ($row as $data) {
                ?>
                    <tr>
                    
                        <td><?= $data->nama ?></td>
                        <td><?= $data->nomor_invoice ?></td>
                        <td><?= $data->tanggal_checkout ?></td>
                        <td><?= $data->no_kamar ?></td>
                        <td><?= $data->nama_tamu ?></td>
                        <td><?= $data->total_biaya_kamar ?></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
            </div>
            <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->