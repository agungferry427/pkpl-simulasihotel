<section class="content">
    <div class="card">
        
                <!-- invoice -->
            <!-- Main content -->
            <div class="p-3 mb-3">
                <!-- title row -->
                <div class="row">
                    <div class="col-12">
                    <h4>
                         Invoice
                         <hr>
                        <small class="float-right"><?php echo date('d-m-Y'); ?></small>
                    </h4>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    To
                        <address>
                            <strong><?=$row->nama_tamu?></strong><br>
                            <?=$row->alamat?><br>
                            Phone: <?=$row->hp_tamu?><br>
                            Email: <?=$row->email_tamu?>
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
               
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b><?=$row->nomor_invoice?></b><br>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <?php
                                $x = strtotime($row->tanggal_checkout);
                                $y = strtotime($row->tanggal_checkin);
                                $hari = abs(($x-$y)/(60*60*24));

                            ?>
                            <tr>
                                <th>Keterangan Layanan/Produk</th>
                                <th>Harga</th>
                                <th>qty</th>
                                <th>Total</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>Room Reserved Type : <?=$row->nama_tipe?></td>
                                <td>Rp. <?=number_format($row->harga_malam,0,',','.')?></td>
                                <td><?=$hari?></td>
                                <?php $hargakamar = $row->harga_malam * $hari ?>
                                <td>Rp. <?=number_format($hargakamar,0,',','.')?></td>
                                <input type="hidden" name="harga_kamar" value="<?=$hargakamar?>">
                            </tr>
                            <?php
                            $harga_total = 0;
                            foreach ($layanan->result() as $key => $data) {
                            ?>
                            <tr>
                                <td><?=$data->nama_layanan?></td>
                                <td>Rp. <?=number_format($data->harga_layanan,0,',','.')?></td>
                                <td><?=$data->jumlah?></td>
                                <td>Rp. <?=number_format($data->total,0,',','.')?></td>
                                <?php $harga_total += $data->total?>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-6">

                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                    <p class="lead"><?php echo date('d-m-Y'); ?></p>

                    <div class="table-responsive">
                        <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal   :</th>
                            <?php $sub_total = $harga_total+$hargakamar?>
                            <td>Rp. <?=number_format($sub_total,0,',','.')?></td>
                            
                        </tr>
                        <tr>
                            <th>PPn 10% :</th>
                            <?php $ppn = $sub_total*0.1?>
                            <td>Rp. <?=number_format($ppn,0,',','.')?></td>
                        </tr>
                        <tr>
                            <th>Deposit :</th>
                            <td>Rp. <?=number_format($row->deposit,0,',','.') ?></td>
                            <input type="hidden" name="deposit" value=<?=$row->deposit?>>
                        </tr>
                        <tr>
                            <th>Grand Total :</th>
                            <?php $grand_total = ($ppn + $sub_total)-$row->deposit ?>
                            <td>Rp. <?=number_format($grand_total,0,',','.') ?></td>
                        </tr>
                        </table>
                    </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <script>
		window.print();
	</script>
              
    </div>
</section>