
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Check out</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">check out</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<section class="content">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">KAMAR NOMOR : <b><?= $row->no_kamar ?></b></h3>
        </div>
        <form action="<?= site_url('checkin/procces') ?>" method="post">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label># INVOICE</label>
                            <input type="hidden" name='statuskamar' value="Vacant Dirty">
                            <input type="hidden" name='id' value="<?= $row->id_kamar ?>">
                            <input type="hidden" name='iduser' value="<?= $this->fungsi->user_login()->id_user ?>">
                            <input type="hidden" name='idt' value="<?= $row->id_transaksi_kamar ?>">
                            <input type="hidden" name='statustamu' value="Check-out">
                            <input class="form-control" readonly name="nomor_invoice" value="<?=$row->nomor_invoice?>" />
                        </div>
                        <div class="alert alert-info">
                            <h4><?= $row->nama_tipe ?></h4>
                            <ul class="list-unstyled">
                                <li>Harga / Malam : <b><?= $row->harga_malam ?></b></li>
                                <li>Maximal Orang Dewasa : <b><?= $row->max_dewasa ?></b></li>
                                <li>Maximal Anak-anak : <?= $row->max_anak ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Nama Tamu</label>
                            <select readonly class="form-control" name="tamuinap">
                                <option value="" readonly>--Pilih--</option>
                                <?php foreach($tamu->result() as $key => $data) {?>
                                <option  value="<?=$data->id_tamu?>" <?=$data->id_tamu == $row->id_tamu ? "selected":null ?>><?=$data->nama_tamu?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="well">
                            <a href="<?= site_url('tamu/add') ?>"><b>Klik disini</b></a> jika nama tamu yang dimaksud tidak ditemukan untuk ditambah pada daftar buku tamu.
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Jumlah Tamu</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <select class="form-control" name="jumlah_dewasa" readonly>
                                        <option value="0">- Dewasa -</option>
                                        <option value="1" <?php echo ($row->jumlah_dewasa== '1' ? ' selected' : ''); ?>>1 Orang</option>
                                        <option value="2" <?php echo ($row->jumlah_dewasa== '2' ? ' selected' : ''); ?>>2 Orang</option>
                                        <option value="3" <?php echo ($row->jumlah_dewasa== '3' ? ' selected' : ''); ?>>3 Orang</option>
                                        <option value="4" <?php echo ($row->jumlah_dewasa== '4' ? ' selected' : ''); ?>>4 Orang</option>
                                        <option value="5" <?php echo ($row->jumlah_dewasa== '5' ? ' selected' : ''); ?>>5 Orang</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select class="form-control" name="jumlah_anak" readonly>
                                        <option value="0">- Anak-Anak -</option>
                                        <option value="1" <?php echo ($row->jumlah_anak== '1' ? ' selected' : ''); ?>>1 Orang</option>
                                        <option value="2" <?php echo ($row->jumlah_anak== '2' ? ' selected' : ''); ?>>2 Orang</option>
                                        <option value="3" <?php echo ($row->jumlah_anak== '3' ? ' selected' : ''); ?>>3 Orang</option>
                                        <option value="4" <?php echo ($row->jumlah_anak== '4' ? ' selected' : ''); ?>>4 Orang</option>
                                        <option value="5" <?php echo ($row->jumlah_anak== '5' ? ' selected' : ''); ?>>5 Orang</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal / Waktu Check-In</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input class="form-control" type="date"name="tanggal_checkin" value="<?=$row->tanggal_checkin ?>" readonly />
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control" name="waktu_checkin" value="<?=$row->waktu_checkin  ?>" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal / Waktu Check-Out</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input id="checkout" type="date" class="form-control" readonly name="tanggal_checkout" value="<?=$row->tanggal_checkout ?>" />
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control" name="waktu_checkout" readonly value="12:00" />
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
                <!-- invoice -->
            <!-- Main content -->
            <div class="p-3 mb-3">
                <!-- title row -->
                <div class="row">
                    <div class="col-12">
                    <h4>
                         Invoice
                         <hr>
                        <small class="float-right"><?php echo date('d-m-Y'); ?></small>
                    </h4>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    To
                        <address>
                            <strong><?=$row->nama_tamu?></strong><br>
                            <?=$row->alamat?><br>
                            Phone: <?=$row->hp_tamu?><br>
                            Email: <?=$row->email_tamu?>
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
               
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b><?=$row->nomor_invoice?></b><br>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <?php
                                $x = strtotime($row->tanggal_checkout);
                                $y = strtotime($row->tanggal_checkin);
                                $hari = abs(($x-$y)/(60*60*24));

                            ?>
                            <tr>
                                <th>Keterangan Layanan/Produk</th>
                                <th>Harga</th>
                                <th>qty</th>
                                <th>Total</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>Room Reserved Type : <?=$row->nama_tipe?></td>
                                <td>Rp. <?=number_format($row->harga_malam,0,',','.')?></td>
                                <td><?=$hari?></td>
                                <?php $hargakamar = $row->harga_malam * $hari ?>
                                <td>Rp. <?=number_format($hargakamar,0,',','.')?></td>
                                <input type="hidden" name="harga_kamar" value="<?=$hargakamar?>">
                            </tr>
                            <?php
                            $harga_total = 0;
                            foreach ($layanan->result() as $key => $data) {
                            ?>
                            <tr>
                                <td><?=$data->nama_layanan?></td>
                                <td>Rp. <?=number_format($data->harga_layanan,0,',','.')?></td>
                                <td><?=$data->jumlah?></td>
                                <td>Rp. <?=number_format($data->total,0,',','.')?></td>
                                <?php $harga_total += $data->total?>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-6">

                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                    <p class="lead"><?php echo date('d-m-Y'); ?></p>

                    <div class="table-responsive">
                        <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal   :</th>
                            <?php $sub_total = $harga_total+$hargakamar?>
                            <td>Rp. <?=number_format($sub_total,0,',','.')?></td>
                            
                        </tr>
                        <tr>
                            <th>PPn 10% :</th>
                            <?php $ppn = $sub_total*0.1?>
                            <td>Rp. <?=number_format($ppn,0,',','.')?></td>
                        </tr>
                        <tr>
                            <th>Deposit :</th>
                            <td>Rp. <?=number_format($row->deposit,0,',','.') ?></td>
                            <input type="hidden" name="deposit" value=<?=$row->deposit?>>
                        </tr>
                        <tr>
                            <th>Grand Total :</th>
                            <?php $grand_total = ($ppn + $sub_total)-$row->deposit ?>
                            <td>Rp. <?=number_format($grand_total,0,',','.') ?></td>
                        </tr>
                        </table>
                    </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

              
            </div>
        <!-- akhir invoice -->

            <div class="card-footer">
                <a href="<?= site_url('checkout/print/').$row->id_transaksi_kamar ?>" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                <button class="btn btn-success" name="<?= $page ?>" type="submit"  >Check-out</button>
                <a class="btn btn-warning" href="<?= site_url('checkout') ?>">Batal</a>
            </div>
        </form>
    </div>
</section>