<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tipe Kamar</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Tipe Kamar</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">

      <div class="pull-right">
        <a href="<?= site_url('tipe/add') ?>" class="float-right btn btn-primary">
          <i class="fa fa-user-plus"></i> Create
        </a>
      </div>
      <h3 class="card-title">Data Tipe Kamar</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
      <table class="table table-striped">
        <thead>
          <tr>
            <th style="width: 10px">no</th>
            <th>Nama Tipe Kamar</th>
            <th>Harga Perkamar</th>
            <th>Keterangan</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
          $no = 1;
          foreach ($row->result() as $key => $data) {
          ?>
            <tr>
              <td><?= $no++ ?></td>
              <td><?= $data->nama_tipe ?></td>
              <td><?= $data->harga_malam ?></td>
              <td><?= $data->keterangan ?></td>
              <td widht="160px" class="text-center">
                <a href="<?= site_url('tipe/edit/') . $data->id_tipe ?>" class="btn btn-primary btn-xs">
                  <i class="fa fa-pen"></i> Update
                </a>
                <a href="<?= site_url('tipe/del/') . $data->id_tipe ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                  <i class="fa fa-trash"></i> Hapus
                </a>

              </td>
            </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->