<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Kamar</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Kamar</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <!-- general form elements -->
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title"><?= ucfirst($page) ?>Kamar</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="<?= site_url('kamar/procces') ?>" method="post">
      <div class="card-body">
        <div class="form-group">
          <label>Nomor Kamar</label>
          <input type="hidden" name='id' value="<?= $row->id_kamar ?>">
          <input type="text" name="no_kamar" class="form-control" placeholder="Enter ..." required>
        </div>
        <div class="form-group">
          <label>Tipe Kamar</label>
          <select class="form-control" name="tipe">
            <option value="">--Pilih--</option>
            <?php foreach ($tipe->result() as $key => $data) { ?>
              <option value="<?= $data->id_tipe ?>"><?= $data->nama_tipe ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>Maximal Dewasa</label>
          <input type="number" name="maxdew" class="form-control" placeholder="Enter ...">
        </div>
        <div class="form-group">
          <label>Maximal Anak-anak</label>
          <input type="number" name="maxanak" class="form-control" placeholder="Enter ...">
          <input type="hidden" name='status' value="Vacant Clean Inspected">
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" name="<?= $page ?>" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <!-- /.card -->


</section>
<!-- /.content -->