<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Kamar</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Kamar</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

 <!-- Main content -->
 <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <?php if ($this->fungsi->user_login()->level == 1 || $this->fungsi->user_login()->level == 3 ){?>
                <div class="pull-right">
                    <a href="<?= site_url('kamar/add') ?>" class="float-right btn btn-primary">
                      <i class="fa fa-user-plus"></i> Create
                      </a>
                </div>      
            <?php } ?>
            <h3 class="card-title">Data Kamar</h3>
          </div>
         <!-- /.card-header -->
          <div class="card-body">
            <table id="table1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 10px">no Kamar</th>
                  <th>Tipe Kamar</th>
                  <th>Harga Perkamar</th>
                  <th>Max Dewasa</th>
                  <th>Max Anak</th>
                  <th>Status Kamar</th>
                  <?php if ($this->fungsi->user_login()->level == 1 || $this->fungsi->user_login()->level == 3 ){?>
                  <th>Action</th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>
                <?php
                    $no = 1;
                    foreach ($row->result() as $key => $data) {
                ?>
                    <tr>
                      <td><?= $data->no_kamar ?></td>
                      <td><?= $data->nama_tipe ?></td>
                      <td><?= $data->harga_malam ?></td>
                      <td><?= $data->max_dewasa ?></td>
                      <td><?= $data->max_anak ?></td>
                      <td><?= $data->status_kamar ?></td>
                      <?php if ($this->fungsi->user_login()->level == 1 || $this->fungsi->user_login()->level == 3 ){?>
                      <td widht="160px" class="text-center">
                          <a href="<?= site_url('kamar/edit/') . $data->id_kamar ?>" class="btn btn-primary btn-xs">
                          <i class="fa fa-pen"></i> Update
                          </a>
                          <a href="<?= site_url('kamar/del/') . $data->id_kamar ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                          <i class="fa fa-trash"></i> Hapus
                          </a>

                      </td>
                      <?php } ?>
                    </tr>
                <?php
                }
                ?>
              </tbody>
                   
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->