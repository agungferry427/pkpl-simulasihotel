
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Kamar</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Kamar</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Kamar</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="<?=site_url('kamar/procces')?>" method="post">
                <div class="card-body"> 
                    <div class="form-group">
                            <label>Nomor Kamar</label>
                            <input type="hidden" name='id' value="<?=$row->id_kamar?>">
                            <input type="text" name="no_kamar" class="form-control"  value="<?=$row->no_kamar?>" placeholder="Enter ..." required >
                    </div>
                    <div class="form-group">
                            <label>Tipe Kamar</label>
                            <select class="form-control" name="tipe">
                                <option value="">--Pilih--</option>
                                <?php foreach($tipe->result() as $key => $data) {?>
                                <option value="<?=$data->id_tipe?>" <?=$data->id_tipe == $row->id_tipe ? "selected":null ?>><?=$data->nama_tipe?></option>
                                <?php } ?>
                            </select>
                    </div>
                    <div class="form-group">
                            <label>Maximal Dewasa</label>
                            <input type="number" name="maxdew" value="<?=$row->max_dewasa?>" class="form-control"  placeholder="Enter ...">
                    </div>
                    <div class="form-group">
                            <label>Maximal Anak-anak</label>
                            <input type="number" name="maxanak" value="<?=$row->max_anak?>"  class="form-control"  placeholder="Enter ...">
                    </div>
                    <div class="form-group">
                            <label>Status Kamar</label>
                            <select class="form-control" name="status">
                                <option value="">--Pilih--</option>
                                <option value="Vacant Clean Inspected" <?php echo ($row->status_kamar == 'Vacant Clean Inspected' ? ' selected' : ''); ?> >Vacant Clean Inspected</option>
                                <option value="Vacant Clean" <?php echo ($row->status_kamar  == 'Vacant Clean' ? ' selected' : ''); ?> >Vacant Clean</option>
                                <option value="Vacant Dirty" <?php echo ($row->status_kamar  == 'Vacant Dirty' ? ' selected' : ''); ?> >Vacant Dirty</option>
                                <option value="Occupied" <?php echo ($row->status_kamar  == 'Occupied' ? ' selected' : ''); ?> >Occupied</option>
                                <option value="Occupied Clean" <?php echo ($row->status_kamar  == 'Occupied Clean' ? ' selected' : ''); ?> >Occupied Clean</option>
                                <option value="Occupied Dirty" <?php echo ($row->status_kamar  == 'Occupied Dirty' ? ' selected' : ''); ?> >Occupied Dirty</option>
                               <option value="Out of Order" <?php echo ($row->status_kamar  == 'Out of Order' ? ' selected' : ''); ?> >Out of Order</option>
                            </select>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" name="<?=$page?>"class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card -->


</section>
<!-- /.content -->
 