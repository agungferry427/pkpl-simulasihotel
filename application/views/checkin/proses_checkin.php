<!-- <?php
        date_default_timezone_set('Asia/Jakarta');
        $nomor_invoice = 'INV-' . date('Ymd') . '-' . (rand(10, 100));
        ?> -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Check In</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">check in</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<section class="content">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">KAMAR NOMOR : <b><?= $row->no_kamar ?></b></h3>
        </div>
        <form action="<?= site_url('checkin/procces') ?>" method="post">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label># INVOICE</label>
                            <input type="hidden" name='id' value="<?= $row->id_kamar ?>">
                            <input type="hidden" name='statuskamar' value="Occupied">
                            <input type ="hidden"name="tanggal_besok" value="<?=date('Y-m-d', time() + (60 * 60 * 24))  ?>" readonly />     
                            <input type="hidden" name='statuskamarcoba' value="Occupied Dirty">
                            <input type="hidden" name='iduser' value="<?= $this->fungsi->user_login()->id_user ?>">
                            <input type="hidden" name='idt' value="<?= $penting->id_transaksi_kamar ?>">
                            <input type="hidden" name='statustamu' value="Check-in">
                            <input class="form-control" name="nomor_invoice" value="<?php echo $nomor_invoice; ?>" />
                        </div>
                        <div class="alert alert-info">
                            <h4><?= $row->nama_tipe ?></h4>
                            <ul class="list-unstyled">
                                <li>Harga / Malam : <b><?= $row->harga_malam ?></b></li>
                                <li>Maximal Orang Dewasa : <b><?= $row->max_dewasa ?></b></li>
                                <li>Maximal Anak-anak : <?= $row->max_anak ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Nama Tamu</label>
                            <select class="form-control" name="tamuinap">
                                <option value="">--Pilih--</option>
                                <?php foreach ($tamu->result() as $key => $data) { ?>
                                    <option value="<?= $data->id_tamu ?>"><?= $data->nama_tamu ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="well">
                            <a href="<?= site_url('tamu/add') ?>"><b>Klik disini</b></a> jika nama tamu yang dimaksud tidak ditemukan untuk ditambah pada daftar buku tamu.
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Jumlah Tamu</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <select class="form-control" name="jumlah_dewasa">
                                        <option value="0">- Dewasa -</option>
                                        <option value="1">1 Orang</option>
                                        <option value="2">2 Orang</option>
                                        <option value="3">3 Orang</option>
                                        <option value="4">4 Orang</option>
                                        <option value="5">5 Orang</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select class="form-control" name="jumlah_anak">
                                        <option value="0">- Anak-anak -</option>
                                        <option value="1">1 Orang</option>
                                        <option value="2">2 Orang</option>
                                        <option value="3">3 Orang</option>
                                        <option value="4">4 Orang</option>
                                        <option value="5">5 Orang</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal / Waktu Check-In</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input class="form-control" name="tanggal_checkin" value="<?php echo date('Y-m-d'); ?>" readonly />
                                </div>
                         
                                <div class="col-sm-6">
                                    <input class="form-control" name="waktu_checkin" value="<?php echo date('H:i'); ?>" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal / Waktu Check-Out</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input id="checkout" type="date" class="form-control" name="tanggal_checkout" data-date-format="yyyy-mm-dd" />
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control" name="waktu_checkout" value="12:00" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jumlah Deposit (Rp)</label>
                            <input class="form-control" name="deposit" required />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <!-- <input type="hidden" name="id_kamar" value="<?php echo $kamar_view['id_kamar']; ?>" /> -->
                <button class="btn btn-success" name="<?= $page ?>" type="submit" name="checkin">Check In</button>
                <a class="btn btn-warning" href="<?= site_url('checkin') ?>">Batal</a>
            </div>
        </form>
    </div>
</section>