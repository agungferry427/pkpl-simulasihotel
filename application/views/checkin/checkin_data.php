<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tamu In Room</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Tamu in Room</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Tamu In Room</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body ">
          <table id="table1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No Kamar</th>
                <th>Nama Tamu</th>
                <th>Tanggal Check-In</th>
                <th>Tanggal Check-Out</th>
                <th>Jumlah Deposit</th>
                <?php if ($this->fungsi->user_login()->level == 1 || $this->fungsi->user_login()->level == 2 ){?>
                <th>Action</th>
                <?php } ?>
              </tr>
            </thead>

            <tbody>
              <?php
              foreach ($row->result() as $key => $data) {
                if ($data->status == "Check-in") {
              ?>
                  <tr>
                    <td><?= $data->no_kamar ?></td>
                    <td><?= $data->prefix ?> <?= $data->nama_tamu ?></td>
                    <td><?= $data->tanggal_checkin ?></td>
                    <td><?= $data->tanggal_checkout ?></td>
                    <td><?= $data->deposit ?></td>
                    <?php if ($this->fungsi->user_login()->level == 1 || $this->fungsi->user_login()->level == 2 ){?>
                    <td widht="160px" class="text-center">
                      <a href="<?= site_url('checkin/edit/') . $data->id_transaksi_kamar ?>" class="btn btn-primary btn-xs">
                        <i class="fa fa-pen"></i> Update
                      </a>
                    <?php }?>
                    </td>
                  </tr>
              <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.card -->
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->