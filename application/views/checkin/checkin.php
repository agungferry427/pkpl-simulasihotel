<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Kamar</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Kamar</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
    foreach ($row->result() as $key => $data) {
      if ($data->status_kamar == "Vacant Clean Inspected") {
    ?>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>
                <td><?= $data->no_kamar ?></td>
              </h3>
              <p>
                <td><?= $data->nama_tipe ?></td>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-bed"></i>
            </div>
            <a href="<?= site_url('checkin/add/') . $data->id_kamar ?>" class="small-box-footer">Pilih Kamar <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
    <?php }
    } ?>

  </div>
  <!-- /.card -->
</section>
<!-- /.content -->