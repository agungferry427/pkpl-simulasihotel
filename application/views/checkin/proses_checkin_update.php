
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Check In</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">check in</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<section class="content">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">KAMAR NOMOR : <b><?= $row->no_kamar ?></b></h3>
        </div>
        <form action="<?= site_url('checkin/procces') ?>" method="post">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label># INVOICE</label>
                            <input type="hidden" name='id' value="<?= $row->id_kamar ?>">
                            <input type="hidden" name='iduser' value="<?= $this->fungsi->user_login()->id_user ?>">
                            <input type="hidden" name='idt' value="<?= $row->id_transaksi_kamar ?>">
                            <input type="hidden" name='statustamu' value="Check-in">
                            <input type="hidden" name='harga_kamar' value="0">
                            <input type="hidden" name='statuskamar'>
                            <input class="form-control" name="nomor_invoice" value="<?=$row->nomor_invoice?>" />
                        </div>
                        <div class="alert alert-info">
                            <h4><?= $row->nama_tipe ?></h4>
                            <ul class="list-unstyled">
                                <li>Harga / Malam : <b><?= $row->harga_malam ?></b></li>
                                <li>Maximal Orang Dewasa : <b><?= $row->max_dewasa ?></b></li>
                                <li>Maximal Anak-anak : <?= $row->max_anak ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Nama Tamu</label>
                            <select class="form-control" name="tamuinap">
                                <option value="">--Pilih--</option>
                                <?php foreach($tamu->result() as $key => $data) {?>
                                <option value="<?=$data->id_tamu?>" <?=$data->id_tamu == $row->id_tamu ? "selected":null ?>><?=$data->nama_tamu?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="well">
                            <a href="<?= site_url('tamu/add') ?>"><b>Klik disini</b></a> jika nama tamu yang dimaksud tidak ditemukan untuk ditambah pada daftar buku tamu.
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Jumlah Tamu</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <select class="form-control" name="jumlah_dewasa">
                                        <option value="0">- Dewasa -</option>
                                        <option value="1" <?php echo ($row->jumlah_dewasa== '1' ? ' selected' : ''); ?>>1 Orang</option>
                                        <option value="2" <?php echo ($row->jumlah_dewasa== '2' ? ' selected' : ''); ?>>2 Orang</option>
                                        <option value="3" <?php echo ($row->jumlah_dewasa== '3' ? ' selected' : ''); ?>>3 Orang</option>
                                        <option value="4" <?php echo ($row->jumlah_dewasa== '4' ? ' selected' : ''); ?>>4 Orang</option>
                                        <option value="5" <?php echo ($row->jumlah_dewasa== '5' ? ' selected' : ''); ?>>5 Orang</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select class="form-control" name="jumlah_anak">
                                        <option value="0">- Anak-Anak -</option>
                                        <option value="1" <?php echo ($row->jumlah_anak== '1' ? ' selected' : ''); ?>>1 Orang</option>
                                        <option value="2" <?php echo ($row->jumlah_anak== '2' ? ' selected' : ''); ?>>2 Orang</option>
                                        <option value="3" <?php echo ($row->jumlah_anak== '3' ? ' selected' : ''); ?>>3 Orang</option>
                                        <option value="4" <?php echo ($row->jumlah_anak== '4' ? ' selected' : ''); ?>>4 Orang</option>
                                        <option value="5" <?php echo ($row->jumlah_anak== '5' ? ' selected' : ''); ?>>5 Orang</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal / Waktu Check-In</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input class="form-control" name="tanggal_checkin" value="<?=$row->tanggal_checkin ?>" readonly />
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control" name="waktu_checkin" value="<?=$row->waktu_checkin  ?>" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal / Waktu Check-Out</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input id="checkout" type="date" class="form-control" name="tanggal_checkout" value="<?=$row->tanggal_checkout ?>" />
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control" name="waktu_checkout" value="12:00" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jumlah Deposit (Rp)</label>
                            <input class="form-control" name="deposit" value="<?=$row->deposit?>" required />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success" name="<?= $page ?>" type="submit">Update</button>
                <a class="btn btn-warning" href="<?= site_url('checkin/tampil_data') ?>">Batal</a>
            </div>
        </form>
    </div>
</section>