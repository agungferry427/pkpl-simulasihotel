<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tamu</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Tamu</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">

          <div class="pull-right">
            <a href="<?= site_url('tamu/add') ?>" class="float-right btn btn-primary">
              <i class="fa fa-user-plus"></i> Create
            </a>
          </div>
          <h3 class="card-title">Data Tamu</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="table1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width: 10px">no</th>
                <th>Nama Tamu</th>
                <th>Warga Negara</th>
                <th>No HP</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <?php
              $no = 1;
              foreach ($row->result() as $key => $data) {
              ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $data->prefix ?> <?= $data->nama_tamu ?></td>
                  <td><?= $data->wn_tamu ?></td>
                  <td><?= $data->hp_tamu ?></td>
                  <td><?= $data->email_tamu ?></td>
                  <td widht="160px" class="text-center">
                    <a href="<?= site_url('tamu/edit/') . $data->id_tamu ?>" class="btn btn-primary btn-xs">
                      <i class="fa fa-pen"></i> Update
                    </a>
                    <a href="<?= site_url('tamu/del/') . $data->id_tamu ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                      <i class="fa fa-trash"></i> Hapus
                    </a>

                  </td>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->