<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
                <div class="row mb-2">
                        <div class="col-sm-6">
                                <h1>Tamu</h1>
                        </div>
                        <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item active">Tamu</li>
                                </ol>
                        </div>
                </div>
        </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
        <!-- general form elements -->
        <div class="card card-primary">
                <div class="card-header">
                        <h3 class="card-title"><?= ucfirst($page) ?> Tamu</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                
                        <div class="card">
                        <form  action="<?=site_url('tamu/procces')?>" method="post">
                                        <div class="card-body">
                                                <div class="row">
                                                        <div class="col-sm-6">
                                                                <div class="form-group">
                                                                        <label>Nama Tamu</label>
                                                                        <div class="row">
                                                                                <div class="col-sm-3">
                                                                                        <select class="form-control" name="panggilan">
                                                                                                <option value="Mr" c>Mr</option>
                                                                                                <option value="Mrs" <?php echo ($row->prefix == 'Mrs' ? ' selected' : ''); ?>>Mrs</option>
                                                                                                <option value="Ms" <?php echo ($row->prefix == 'Ms' ? ' selected' : ''); ?>>Ms</option>
                                                                                        </select>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                        <input type="hidden" name='id' value="<?= $row->id_tamu ?>">
                                                                                        <input type="text" name="namatamu" class="form-control" value="<?= $row->nama_tamu ?>" placeholder="Enter ..." required>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label>Identitas</label>
                                                                        <div class="row">
                                                                                <div class="col-sm-3">
                                                                                        <select class="form-control" name="identitas">
                                                                                                <option value="KTP" <?php echo ($row->tipe_identitas == 'KTP' ? ' selected' : ''); ?>>KTP</option>
                                                                                                <option value="SIM" <?php echo ($row->tipe_identitas == 'SIM' ? ' selected' : ''); ?>>SIM</option>
                                                                                                <option value="PASSPORT" <?php echo ($row->tipe_identitas == 'PASSPORT' ? ' selected' : ''); ?>>PASSPORT</option>
                                                                                        </select>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                        <input type="text" name="noi" class="form-control" value="<?= $row->no_identitas ?>" placeholder="Enter ...">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <label>Status Warga Negara</label>
                                                                        <div class="row">
                                                                                <div class="col-sm-6">
                                                                                        <input type="text" name="wn" value="<?= $row->wn_tamu ?>" class="form-control" placeholder="Enter ...">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="col-sm-5">
                                                                <div class="form-group">
                                                                        <label>Alamat</label>
                                                                        <textarea name="alamat" class="form-control"><?= $row->alamat ?></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="row">
                                                                                <div class="col-sm-6">
                                                                                        <label>Nomor Telp / Handphone</label>
                                                                                        <input type="text" name="nohp" value="<?= $row->hp_tamu ?>" class="form-control" placeholder="Enter ...">
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                        <label>Email</label>
                                                                                        <input type="email" name="email" value="<?= $row->email_tamu ?>" class="form-control" placeholder="Enter ...">
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="card-footer">
                                                <button type="submit" name="<?= $page ?>" class="btn btn-primary">Submit</button>
                                         </div>
                                </form>
                        </div>
                        <!-- /.card-body -->
                   
               
        </div>
        <!-- /.card -->


</section>
<!-- /.content -->