<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Dashboard</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3><?= $tamu ?></h3>

          <p>Total tamu</p>
        </div>
        <div class="icon">
          <i class="fas fa-user"></i>
        </div>
        <a href="<?= site_url('kamar') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3><?php echo $dataif->VCI ?></h3>
          <p>VCI</p>
        </div>
        <div class="icon">
          <i class="fas fa-bed"></i>
        </div>
        <a href="<?= site_url('kamar') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3><?php echo $dataif->OCC ?></h3>

          <p>Occupied</p>
        </div>
        <div class="icon">
          <i class="fas fa-bed"></i>
        </div>
        <a href="<?= site_url('kamar') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3><?php echo $user ?></h3>

          <p>Total User</p>
        </div>
        <div class="icon">
          <i class="fas fa-user"></i>
        </div>
        <a href="<?= site_url('kamar') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.card -->
  <div class="row">
    <div class="col-md-6">
      <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tamu in-room</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">#Kamar</th>
                    <th>Nama</th>
                    <th>Tanggal check-in</th>
                  
                  </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($trankamar->result() as $key => $data) {
                      if ($data->status == "Check-in") {
                    ?>
                    <tr>
                      <td><?= $data->no_kamar ?></td>
                      <td><?= $data->prefix ?> <?= $data->nama_tamu ?></td>
                      <td><?= $data->tanggal_checkin ?></td>
                    </tr>
                    <?php
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <div class="col-md-6">
      <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tamu Check-out hari ini</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">#Kamar</th>
                    <th>Nama</th>
                    <th>Tanggal check-out</th>
                  
                  </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($trankamar->result() as $key => $data) {
                      if ($data->status == "Check-in" && $data->tanggal_checkout == date('Y-m-d') ) {
                    ?>
                    <tr>
                      <td><?= $data->no_kamar ?></td>
                      <td><?= $data->prefix ?> <?= $data->nama_tamu ?></td>
                      <td><?= $data->tanggal_checkout ?></td>
                    </tr>
                    <?php
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
                  
</section>
<!-- /.content -->